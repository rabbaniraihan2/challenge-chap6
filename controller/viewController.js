const {user_games} = require('../models')

module.exports = class ViewController {
    static async home (req,res) {
        try {
            const data = await user_games.findAll()
            res.render('./home', { data })
        } catch (error) {
            res.send('error')
        }
    }

    static async register (req,res) {
        try {
            res.render('./register')
        } catch (error) {
            res.send('error')
        }
    }

    static async edit (req,res) {
        try {
            const id = req.params.id
            const data = await user_games.findOne({
                where : {
                    id
                }
            })
            res.render('./edit',{data})
        } catch (error) {
            res.send('error')
        }
    }

    static async login (req,res) {
        try {
            res.render('./login')
        } catch (error) {
            res.send('error')
        }
    }
}