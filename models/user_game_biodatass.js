'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_biodatass extends Model {
    static associate(models) {
      user_game_biodatass.belongsTo(models.user_games, {foreignKey : 'usernameId'})
    }
  }
  user_game_biodatass.init({
    name: DataTypes.STRING,
    usernameId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user_game_biodatass',
  });
  return user_game_biodatass;
};